<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xslt="http://xml.apache.org/xslt" xmlns:ser="http://services.oam.xcel.com">
	<xsl:output method="xml" indent="yes" omit-xml-declaration="yes" xslt:indent-amount="2"/>
	<xsl:strip-space elements="*"/>
	<xsl:param name="usernameTokenId" select="''"/>
	<xsl:param name="username" select="''"/>
	<xsl:param name="password" select="''"/>
	<xsl:param name="requestId" select="''"/>
	<xsl:param name="accountNumber" select="''"/>

	<xsl:template match="/soapenv:Envelope">
		<soapenv:Envelope>
			<soapenv:Header>
				<xsl:choose>
					<xsl:when test="soapenv:Header">
						<xsl:apply-templates select="soapenv:Header"/>
					</xsl:when>
					<xsl:otherwise>
						<wsse:Security>
							<xsl:call-template name="injectUsernameToken"/>
						</wsse:Security>
					</xsl:otherwise>
				</xsl:choose>
			</soapenv:Header>
			<soapenv:Body>
				<xsl:apply-templates select="soapenv:Body"/>
			</soapenv:Body>
		</soapenv:Envelope>
	</xsl:template>
	<xsl:template match="soapenv:Body">
		<ser:retrieveStatementList>
			<requestElement>
			    <endUser>
                    <endUserID>Apigee</endUserID>
                    <endUserType>A</endUserType>
                </endUser>
				<requestID>
					<xsl:value-of select="$requestId"/>
				</requestID>
				<accountNumber>
				    <xsl:value-of select="$accountNumber"/>
				</accountNumber>
			</requestElement>
		</ser:retrieveStatementList>
	</xsl:template>
	<xsl:template match="soapenv:Header">
		<xsl:choose>
			<xsl:when test='wsse:Security'>
				<xsl:apply-templates select="*"/>
			</xsl:when>
			<xsl:otherwise>
				<wsse:Security>
					<xsl:call-template name="injectUsernameToken"/>
				</wsse:Security>
				<xsl:copy-of select="*"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()"/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="soapenv:Header/wsse:Security/wsse:UsernameToken"/>
	<xsl:template name='injectUsernameToken'>
		<wsse:UsernameToken>
			<xsl:attribute name="wsu:Id"><xsl:value-of select="$usernameTokenId"/></xsl:attribute>
			<wsse:Username>
				<xsl:value-of select="$username"/>
			</wsse:Username>
			<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">
				<xsl:value-of select="$password"/>
			</wsse:Password>
		</wsse:UsernameToken>
	</xsl:template>
</xsl:stylesheet>
