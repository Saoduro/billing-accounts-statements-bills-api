var response = context.getVariable("response.content");
var body = JSON.parse(response);

var listResponse = body.Envelope.Body.retrieveStatementListResponse;
var pdfResponse = body.Envelope.Body.retrieveStatementContentsByType;


if (listResponse){
    context.setVariable("listResponse", "true");
    context.setVariable("contentType", "application/json"); 
}
else
{
        context.setVariable("PDFresponse", "true");
        context.setVariable("contentType", "application/pdf"); 
}