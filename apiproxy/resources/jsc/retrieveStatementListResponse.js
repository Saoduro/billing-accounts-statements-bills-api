var response = context.getVariable("soapResponse.body");
if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    var responseCode = responseBody.applicationError.code;
    var checkDigitId = responseBody.accountNumber.toString();

    var responseObject = {};
    var pdfStatements = [];
    var finalObject = {};
    var afsErrorCodes = {
        "P55": "401",  //Failed Authentication
        "100": "400",  //Missing Required Field"
        "101": "400",  //Invalid Field Length
        "104": "400",  //Invalid Premise Number
        "105": "404",  //No Account Found
        "112": "401",  //Invalid User Role
        "204": "404"   //Invalid Statement Date and Number Format
    };

    if (responseCode == 1) {
        context.setVariable('response.status.code', 200);
        var correlationId = context.getVariable("correlationId");

        // map correlationId
        if (responseBody.requestID != "NULL") {
            responseObject.correlationId = correlationId;
        } else {
            responseObject.correlationId = "";
        }

        // map checkDigitBillingId
        if (responseBody.accountNumber != "NULL") {
            responseObject.checkDigitBillingId = checkDigitId;
        } else {
            responseObject.checkDigitBillingId = "";
        }

        //map statement list
        var eBillStatementArray = [];
        

        // map pdfStatements->number, date and hasPDF
        var eBillStatementElement = null;
        if (responseBody.EBillStatements != null)
            eBillStatementElement = responseBody.EBillStatements.EBillStatementElement;
            
        if(eBillStatementElement != null){
            for(var i=0; i < eBillStatementElement.length; i++){
                var eBillStatementObject = {};
    
                eBillStatementObject.number=eBillStatementElement[i].number;
                eBillStatementObject.date=eBillStatementElement[i].date;
                
                if (eBillStatementElement[i].pdf === 0)
                    eBillStatementObject.hasPDF=false;
                else
                    eBillStatementObject.hasPDF=true;
                    
                eBillStatementArray.push(eBillStatementObject);
            }
        }
        
        responseObject.pdfStatements = eBillStatementArray;
        context.setVariable("isValidResponse", true);

        //pdfStatements.push(finalObject);
        //responseObject.pdfStatements = pdfStatements;

    } else {
        var responseMessage = responseBody.applicationError.logMessage;
        var responsetype = responseBody.applicationError.type;
        var responseCode = responseBody.applicationError.code;
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", responseMessage);
        context.setVariable("responsetype", responsetype);
        if (typeof responseCode != 'undefined' && responseCode !== "NULL"){
            context.setVariable("responseCode", afsErrorCodes[responseCode]);
        }
    }


    context.setVariable("finalResponse", JSON.stringify(responseObject));


}