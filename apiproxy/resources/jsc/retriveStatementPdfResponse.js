var response = context.getVariable("soapResponse.body");
if (typeof response != 'undefined') {
    var responseBody = JSON.parse(response);
    var responseCode = responseBody.applicationError.code;
 

    var responseObject = {};
    var finalObject = {};
    var afsErrorCodes = {
        "P55": "401",  //Failed Authentication
        "100": "400",  //Missing Required Field"
        "101": "400",  //Invalid Field Length
        "104": "400",  //Invalid Premise Number
        "105": "404",  //No Account Found
        "112": "401",  //Invalid User Role
        "204": "404"   //Invalid Statement Date and Number Format
    };

    if (responseCode == 1) {
        var EBillStatementContent = responseBody.EBillStatements.EBillStatementElement.EBillStatementContent;
        var checkDigitId = responseBody.accountNumber.toString();
        var correlationId = context.getVariable("correlationId");
        context.setVariable('response.status.code', 200);

        // map checkDigitBillingId
        if (EBillStatementContent.fileType == "PDF") {
            responseObject = EBillStatementContent.fileContents;
        } else {
            responseObject = "";
        }
        context.setVariable("isValidResponse", true);
    } else {
        var responseMessage = responseBody.applicationError.logMessage;
        var responsetype = responseBody.applicationError.type;
        context.setVariable("isValidResponse", false);
        context.setVariable("responseMessage", responseMessage);
        context.setVariable("responsetype", responsetype);
        if (typeof responseCode != 'undefined' && responseCode !== "NULL"){
            context.setVariable("responseCode", afsErrorCodes[responseCode]);
        }
    }
    context.setVariable("finalResponse", JSON.stringify(responseObject));
}
